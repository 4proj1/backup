#Home

##/!\ RESTART DOCKER DESKTOP IF CONTAINER TIME IS WRONG /!\

##How does it works ?

It copy, each day at 2 AM, a local folder to a remote AWS S3 Bucket.

##How to use ?

You have to populate some environment variables : 
- ACCESS_KEY : AWS access key of your account
- SECRET_KEY : AWS secret key of your account
- S3_NAME : name of your AWS S3 Bucket
- LOCAL_PATH : path of the folder you want to copy

You will have to mount a volume if you want to copy files from the host.
FROM python

RUN pip install boto3
RUN apt-get update && apt-get install -y cron

ENV PYTHONPATH "/app/project"

COPY . /project/

WORKDIR /project/
RUN chmod 777 /project/entrypoint.sh

#CMD ["python", "-u", "backup.py"]
ENTRYPOINT /project/entrypoint.sh
import os
from datetime import datetime

import boto3

S3_NAME = os.getenv('S3_NAME')

ACCESS_KEY = os.getenv('ACCESS_KEY')
SECRET_KEY = os.getenv('SECRET_KEY')
LOCAL_PATH = os.getenv('LOCAL_PATH')


def send_to_s3(local_path, remote_path):
    s3 = boto3.client('s3', aws_access_key_id=ACCESS_KEY,
                      aws_secret_access_key=SECRET_KEY, region_name='eu-west-3')

    with open(local_path, "rb") as file_to_send:
        s3.upload_fileobj(file_to_send, S3_NAME, remote_path)


timestamp = datetime.timestamp(datetime.now())
print(timestamp)

files = []

for r, d, f in os.walk(LOCAL_PATH):
    for file in f:
        result_path = str(timestamp) + "/" + os.path.join(r, file)
        print(os.path.join(r, file))
        print(result_path)
        send_to_s3(os.path.join(r, file), result_path)
